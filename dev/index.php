<?php
/**
 * listado de status, 0 fuera de linea, 1 modo productivo, 2 mod prueba, 3 modo desarrollo, 4 modo suspension
 */
$status = 3;
switch ($status)
{
    case 1:
        $TitlePage = "la conexion es contigo";
        $ContentPage = "Bienvendio";
        break;
    case 2:
        $TitlePage = "Modo prueba activado";
        $ContentPage = "Modo Pruba";
        break;
    case 3:
        $TitlePage  = "Modo desarrollo activado";
        require_once 'pages/OnLine.php';
        $class = new Online();        
        $ContentPage = $class->showPage();
        break;
    case 4:
        $TitlePage = "Sitio supendedido";
        $ContentPage = "El sitio a sido suspendido temporalmente";
        break;
    default :
        $TitlePage;
        $ContentPage;
}
?>
<!DOCTYPE html>
<html lang="es-MX>
    <head>
    <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>Ancestralli- <?php echo $TitlePage?>-</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">
  <!-- Favicons -->
  <link  rel="icon" href="assets/img/logo.png" type="image/png" />
  <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">       
  <link href="assets/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/plugins/owl-carousel/css/owl.carousel.css" rel="stylesheet">
  <link href="assets/plugins/owl-carousel/css/owl.theme.default.css" rel="stylesheet">
  <link href="assets/plugins/icheck/skins/minimal/blue.css" rel="stylesheet">
  <!--master slider-->
  <link href="assets/plugins/masterslider/style/masterslider.css" rel="stylesheet">
  <link href="assets/plugins/masterslider/skins/default/style.css" rel='stylesheet'>
   <!--template css-->    
   <link href="assets/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="contenido">
            <?php
                echo $ContentPage
            ?>
        </div>        
    </body>
</html>