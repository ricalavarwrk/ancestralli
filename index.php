<!DOCTYPE html>
<html lang="es-MX>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>Ancestralli- la conexion es contigo-</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex flex-column align-items-center">
      <h1>Ancestralli</h1>
      <h2>Estamos trabajando duro para mejorar nuestro sitio web y estaremos listos para lanzarlo después.</h2>
      <div class="countdown d-flex justify-content-center" data-count="2020/11/18">
        <div>
          <h3>%D</h3>
          <h4>Dias</h4>
        </div>
        <div>
          <h3>%H</h3>
          <h4>Horar</h4>
        </div>
        <div>
          <h3>%M</h3>
          <h4>Minutos</h4>
        </div>
        <div>
          <h3>%S</h3>
          <h4>Segundos</h4>
        </div>
      </div>
      <div class="social-links text-center">
        <!--<a href="#" class="twitter"><i class="icofont-twitter"></i></a>-->
        <a href="https://www.facebook.com/Ancestralli-106491151196497/" target="_blanck" class="facebook"><i class="icofont-facebook"></i></a>
        <!--<a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="google-plus"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></a>-->
      </div>
    </div>
  </header><!-- End #header -->
  <main id="main">
    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title">
          <h2>Nosotros</h2>
          <p>El mundo actual, un mundo cambiante, donde lo material parece tener mas importancia que lo espiritual.

La tradiciones se comienzan a olvidar, el respeto por la naturaleza, así como su uso, pero sobre todas las cosas, la conexión con nuestra espiritualidad, se empiezan a perder.

Ancestralli buscar ayudar, a que como seres humanos, volvamos a tener esa conexión con uno mismo.
</p>
        </div>
      </div>
    </section><!-- End About Us Section -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Ancestralli</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/maundy-free-coming-soon-bootstrap-theme/ -->
        Designed by <a href="https://ricalavar.net/" target="_blank">RicAlaVar</a>
      </div>
    </div>
  </footer><!-- End #footer -->
  <a href="#" class="back-to-top" ><i class="icofont-simple-up"></i></a>
  <!-- Vendor JS Files -->
  <script data-cfasync="false" src="assets/js/email-decode.min.js"></script>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-countdown/jquery.countdown.min.js"></script>
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
</body>
</html>